
# esp-lib-manager

This repository contains sources for package manager for espruino libs.


## Installation

Install this utility with npm

```bash
  npm install -D esp-lib-manager
```
## Usage/Examples

```bash
esp-lib-manager -a // Install all modules declared in esp-lib-manager.json
esp-lib-manager -i [names] // Creates esp-lib-manager.json if not exists and
installs modules
esp-lib-manager -u [names] // Uninstalls modules
```

## Contributing

Contributions are always welcome!

You can help to improve types coverage through MR (please use `prettier` and `eslint` config).