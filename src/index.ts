#!/usr/bin/env node

import { text } from '@clack/prompts';
import { Command } from 'commander';
import { mkdir } from 'fs/promises';
import { join } from 'path';

import { MODULES_DIR, MODULES_MANAGER_FILENAME } from './constants';
import {
  ConfigService,
  FileType,
  FSService,
  LoaderService,
  LoggerService,
  LogLevel,
} from './services';
import { IArguments } from './types';

const program = new Command();

program.option('-a, --all', 'install all used modules');
program.option('-i, --install <module-names...>', 'install module package');
program.option('-u, --uninstall <module-names...>', 'uninstall module package');

program.parse(process.argv);

/**
 * Parsing cli arguments
 */
const options = program.opts<Partial<IArguments>>();
const workingDirectory = process.cwd();

const main = async () => {
  const loggerService = new LoggerService();
  const fsService = new FSService(
    join(workingDirectory, MODULES_DIR),
    loggerService
  );
  const configService = new ConfigService(
    `${workingDirectory}/${MODULES_MANAGER_FILENAME}`,
    loggerService
  );

  await configService.loadConfig();

  const loaderService = new LoaderService(configService.config);

  /**
   * Create modules dir if not exists
   */
  if (!fsService.checkExists('')) {
    await mkdir(MODULES_DIR);
  }

  if (options.all || options.install) {
    const modules = options.all
      ? (configService.get('modules') as string[])
      : options.install;

    if (!modules) {
      return loggerService.log('Not found modules to load', LogLevel.ERROR);
    }

    const modulesToLoad = modules.filter(
      (module) => !fsService.checkExists(module)
    );

    if (modulesToLoad.length === 0) {
      return loggerService.log('Nothing to load', LogLevel.INFO);
    }

    const data = await Promise.all(
      modulesToLoad.map((module) => loaderService.upload(module))
    );

    const notFoundModules = data.filter((module) => module.data.length === 0);

    if (notFoundModules.length > 0) {
      return loggerService.log(
        `Modules: ${notFoundModules
          .map(({ moduleName }) => moduleName)
          .join(', ')} not found`,
        LogLevel.ERROR
      );
    }

    const loadedModules = data.filter((module) => module.data.length > 0);

    const modulesToSave = loadedModules.filter(
      (module) => module.data.length === 1 && module.data[0].data
    );

    const mustSelect = loadedModules.filter((module) => module.data.length > 1);

    for (const module of mustSelect) {
      const { moduleName, data } = module;

      const meaning = await text({
        message: `There is many repositories that contains package ${moduleName}, please enter the number of repository: ${data
          .map(({ repoName }, index) => `${index + 1} - ${repoName}`)
          .join(', ')}`,
        initialValue: '1',
        validate(value) {
          if (
            !Number.isInteger(Number(value)) ||
            Number(value) - 1 > data.length
          )
            return `Please enter correct repository number`;
        },
      });

      modulesToSave.push({
        ...module,
        data: [module.data[Number(meaning) - 1]],
      });
    }

    await Promise.all(
      modulesToSave.map(async ({ moduleName, data, declaration }) => {
        try {
          const [{ data: moduleContent }] = data;
          await fsService.save(
            moduleName,
            FileType.ModuleJs,
            moduleContent as string
          );

          if (declaration) {
            await fsService.save(moduleName, FileType.Declaration, declaration);
          }

          loggerService.log(
            `Module ${moduleName} was successfully installed 🚀`
          );
        } catch (e) {
          loggerService.log(
            `An error occurred while install ${moduleName} package`,
            LogLevel.ERROR
          );
        }
      })
    );

    if (options.install) {
      modulesToSave.forEach((module) =>
        configService.addModule(module.moduleName)
      );
    }
  }

  if (options.uninstall) {
    const modules = options.uninstall;
    if (!modules) {
      return loggerService.log('Not found modules to uninstall');
    }

    await Promise.all(modules.map(fsService.delete));
    modules.forEach(configService.removeModule);

    return loggerService.log('Modules was removed');
  }

  return;
};

void main();
