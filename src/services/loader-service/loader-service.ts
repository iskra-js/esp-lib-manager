import axios, { AxiosInstance, AxiosResponse } from 'axios';

import { IConfig } from '../../types';
import { ILoadedModule, ILoaderService } from './types';
import { getDefinitionsPath, getLoadPaths } from './utils';

export class LoaderService implements ILoaderService {
  private readonly axiosClient: AxiosInstance;
  constructor(private readonly config: IConfig) {
    this.axiosClient = axios.create({ method: 'GET' });
  }
  public async upload(name: string): Promise<ILoadedModule> {
    const declaration = this.config.useTypescript
      ? ((
          await this.loadFile(
            getDefinitionsPath(this.config.definitionsRepository, name)
          )
        )?.data as string)
      : undefined;

    const paths = getLoadPaths(this.config.moduleRepositories, name);

    const data = await Promise.all(
      paths.map(async ({ repoName, path }) => {
        const response = await this.loadFile(path);

        return {
          repoName,
          data: response?.data,
        };
      })
    );

    return {
      data: data.filter(({ data }) => data),
      declaration,
      moduleName: name,
    };
  }

  private async loadFile(path: string): Promise<AxiosResponse | undefined> {
    try {
      return await this.axiosClient(path);
    } catch (e) {
      return;
    }
  }
}
