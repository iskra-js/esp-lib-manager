export interface IModuleData {
  repoName: string;
  data: string | undefined;
}

export interface ILoadedModule {
  moduleName: string;
  declaration?: string;
  data: IModuleData[];
}

export interface ILoaderService {
  upload(name: string): Promise<ILoadedModule>;
}
