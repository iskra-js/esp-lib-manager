import { IRepository } from '../../types';

type Path = {
  repoName: string;
  path: string;
};
type LoadPaths = Path[];

export const getDefinitionsPath = (repository: string, moduleName: string) => {
  return `${repository}${moduleName}.d.ts`;
};

export const getLoadPaths = (
  repositories: IRepository[],
  moduleName: string
): LoadPaths =>
  repositories.map((repository) => ({
    repoName: repository.name,
    path: `${repository.path}${moduleName}.js`,
  }));
