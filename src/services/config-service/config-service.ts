import { existsSync, writeFileSync } from 'fs';
import { readFile } from 'fs/promises';

import { INITIAL_CONFIG } from '../../constants';
import { IConfig } from '../../types';
import { ILoggerService, LogLevel } from '../logger-service';
import { IConfigService } from './types';

type JsonFileName = `${string}.json`;

export class ConfigService implements IConfigService {
  private readonly filePath: string;
  public config = {} as IConfig;

  constructor(fileName: JsonFileName, private readonly logger: ILoggerService) {
    this.filePath = fileName;
  }

  public async loadConfig(): Promise<void> {
    const isExists = existsSync(this.filePath);

    if (!isExists) {
      this.logger.log('Config not found, creating...', LogLevel.WARNING);
      this.config = INITIAL_CONFIG;
      await this.saveConfig();
      return;
    }

    const config = await readFile(this.filePath, { encoding: 'utf8' });
    this.config = JSON.parse(config) as IConfig;
  }

  private saveConfig(): void {
    const config = JSON.stringify(this.config, null, 2);
    writeFileSync(this.filePath, config, { flag: 'w', encoding: 'utf8' });
  }

  public get(key: keyof IConfig): IConfig[keyof IConfig] {
    return this.config[key];
  }

  public addModule = (name: string): void => {
    const uniqueModules = [...this.config.modules];
    uniqueModules.push(name);
    this.config.modules = Array.from(new Set(uniqueModules));
    this.saveConfig();
  };

  public removeModule = (name: string): void => {
    const modules = [...this.config.modules];
    this.config.modules = modules.filter((module) => module !== name);
    this.saveConfig();
  };
}
