import { IConfig } from '../../types';

export interface IConfigService {
  loadConfig(): Promise<void>;
  get(key: keyof IConfig): IConfig[keyof IConfig];
  addModule(name: string): void;
  removeModule(name: string): void;
}
