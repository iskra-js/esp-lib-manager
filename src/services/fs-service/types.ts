export enum FileType {
  ModuleJs = 'js',
  Declaration = 'd.ts',
}

export interface IFSService {
  save(name: string, extension: FileType, data: string): Promise<void>;
  checkExists(name: string): boolean;
  delete(name: string): Promise<void>;
}
