import { existsSync } from 'fs';
import { mkdir, rm, writeFile } from 'fs/promises';
import { join } from 'path';

import { ILoggerService, LogLevel } from '../logger-service';
import { IFSService } from './types';

export class FSService implements IFSService {
  private readonly baseDir: string;

  constructor(baseDir: string, private readonly logger: ILoggerService) {
    this.baseDir = baseDir;
  }

  public checkExists(name: string): boolean {
    return existsSync(join(this.baseDir, name));
  }

  public delete = async (name: string): Promise<void> => {
    try {
      await rm(join(this.baseDir, name), { recursive: true, force: true });
    } catch (e) {
      this.logger.log(`Can't remove package: ${name}`, LogLevel.WARNING);
    }
  };

  public save = async (
    name: string,
    extension: string,
    data: string
  ): Promise<void> => {
    if (this.checkExists(join(name, `index.${extension}`))) {
      return;
    }

    if (!this.checkExists(name)) {
      await mkdir(join(this.baseDir, name));
    }
    await writeFile(join(this.baseDir, name, `index.${extension}`), data, {
      encoding: 'utf8',
      flag: 'w',
    });
  };
}
