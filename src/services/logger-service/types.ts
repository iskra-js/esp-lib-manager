export enum LogLevel {
  ERROR = 'ERROR',
  WARNING = 'WARNING',
  INFO = 'INFO',
}

export interface ILoggerService {
  log(message: string, level?: LogLevel): void;
}
