import { ILoggerService, LogLevel } from './types';

export class LoggerService implements ILoggerService {
  public log(message: string, level: LogLevel = LogLevel.INFO): void {
    const date = new Date();
    console.log(`[${level}]: ${date.toLocaleString()} - ${message}`);
  }
}
