export interface IRepository {
  name: string;
  path: string;
}

export interface IConfig {
  modules: string[];
  moduleRepositories: IRepository[];
  definitionsRepository: string;
  useTypescript: boolean;
}
