export interface IArguments {
  all: boolean;
  install: string[];
  uninstall: string[];
}
