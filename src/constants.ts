import { IConfig } from './types';

export const MODULES_DIR = 'esp-libs';
export const MODULES_MANAGER_FILENAME = 'esp-package.json';

const ESPRUINO_REPO =
  'https://raw.githubusercontent.com/espruino/EspruinoDocs/master/modules/';
const ISKRA_REPO =
  'https://raw.githubusercontent.com/amperka/espruino-modcat/master/modules/%40amperka/';
const DEFINITIONS_PATH =
  'https://gitlab.com/iskra-js/declarations/-/raw/main/src/';

export const INITIAL_CONFIG: IConfig = {
  modules: [],
  moduleRepositories: [
    {
      name: 'espruino',
      path: ESPRUINO_REPO,
    },
    {
      name: 'amperka',
      path: ISKRA_REPO,
    },
  ],
  definitionsRepository: DEFINITIONS_PATH,
  useTypescript: true,
};
